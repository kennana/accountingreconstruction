// eslint-disable-next-line
/* eslint-disable */
module.exports = {
    plugins: {
        'autoprefixer': {
            browsers: ['Android >= 4.0', 'iOS >= 7']
        },
        'postcss-pxtorem': {
            rootValue: 37.5,
            propList: ['*']
        },
        'postcss-px2rem': {
            remUnit: 30,
        }
    }
}