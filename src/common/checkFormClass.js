export class CheckFormClass {
    constructor(email, passwd, confirm) {
        this.email = email;
        this.passwd = passwd;
        this.confirm = confirm;
        this.instance = null;
    }

    static init(email, passwd, confirm) {
        this.instance = new CheckFormClass(email, passwd, confirm);
        return this.instance;
    }

    getFormInstance() {
        return this.instance;
    }

    getFormEmail() {
        return this.email;
    }

    getFormPasswd() {
        return this.passwd;
    }

    getFormConfirm() {
        return this.confirm;
    }

    checkEmail() {
        var re = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
        if (re.test(this.email)) {
            return true;
        } else {
            return false;
        }
    }
    checkPasswd() {
        if ((this.passwd !== '')) {
            return true;
        } else {
            return false;
        }
    }

    checkConfirm() {
        if ((this.passwd !== '') && (this.confirm !== '')) {
            if (this.passwd === this.confirm) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;

        }
    }

}