// toast 方法
export function openBottom(context) {
    context.$toast("bottom"); // or this.$toast.bottom('bottom');
}

export function openTop(context) {
    context.$toast.top("top");
}

export function openCenter(context, text) {
    context.$toast.center(text);
}

export function openLoading(context, text) {
    context.$loading(text);
    let self = context;
    setTimeout(function() {
        self.closeLoading(self);
    }, 2000);
}

export function closeLoading(context) {
    context.$loading.close();
}