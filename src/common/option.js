/*ECharts雷达图表*/
var option = {
    textStyle: {
        fontSize: 12,
    },

    query: {
        minWidth: 200,
        maxHeight: 300,
        minAspectRatio: 1.3
    },
    grid: {
        x: "20%"
    },
    // title: {
    //   text: "本年消费趋势图"
    // },
    tooltip: {
        trigger: "axis"
    },
    legend: {
        fontSize: "100%",
        orient: "horizontal", // 布局方式，默认为水平布局，可选为：
        // 'horizontal' ¦ 'vertical'
        x: "center", // 水平安放位置，默认为全图居中，可选为：
        // 'center' ¦ 'left' ¦ 'right'
        // ¦ {number}（x坐标，单位px）
        y: "top", // 垂直安放位置，默认为全图顶端，可选为：
        // 'top' ¦ 'bottom' ¦ 'center'
        data: ["收入", "支出"]
    },
    toolbox: {
        show: true,
        feature: {
            dataView: { show: false, readOnly: false },
            magicType: { show: false, type: ["line", "bar"] },
            restore: { show: false },
            saveAsImage: { show: false }
        }
    },
    calculable: true,
    xAxis: [{
        type: "category",
        fontSize: "100%",
        data: [
            "1月",
            "2月",
            "3月",
            "4月",
            "5月",
            "6月",
            "7月",
            "8月",
            "9月",
            "10月",
            "11月",
            "12月"
        ]
    }],
    yAxis: [{
        type: "value"
    }],
    series: [{
            name: "收入",
            type: "bar",
            fontSize: "100%",
            data: [
                1000,
                2000,
                3000,
                4000,
                5000,
                6000,
                2500,
                1000,
                2300,
                2000,
                6040,
                3030
            ]
        },
        {
            name: "支出",
            type: "bar",
            fontSize: "100%",
            data: [
                2600,
                5090,
                9000,
                2604,
                2807,
                7007,
                1756,
                1822,
                4807,
                1808,
                6000,
                2300
            ]
        }
    ]
};

export {
    option,
}