export function checkEmail(email) {
    var re = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
    if (re.test(email)) {
        return true;
    } else {
        return false;
    }
}


export function checkPasswd(passwd) {
    if (passwd !== '') {
        return true;
    } else {
        return false;
    }
}


export function checkConfirm(passwd, confirm) {
    if (passwd !== '' && confirm !== '') {
        if (passwd === confirm) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}