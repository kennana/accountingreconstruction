var html = document.getElementsByTagName("html")[0];
var width = html.clientWidth;
var tooltipFontSize = 14 / 1280 * width;
var fontSize = 10 / 1280 * width;
var legendFontSize = 10 / 1280 * width;
var labelFontSize = 10 / 1280 * width;
var itemHeight = 8 / 1280 * width;
var itemWidth = 10 / 1280 * width;