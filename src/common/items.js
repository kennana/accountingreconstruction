var items = [{
        cls: "home",
        name: "首页",
        push: "home",
        icon: "iconfont icon-shouye",
        iconSelect: "iconfont icon-shouye"
    },
    {
        cls: "account",
        name: "账本",
        push: "account",
        icon: "iconfont icon-icon-test",
        iconSelect: "iconfont icon-icon-test"
    },
    {
        cla: "addaccount",
        name: "记账",
        push: "addaccount",
        icon: "iconfont icon-jiahao",
        iconSelect: "iconfont icon-jiahao"
    },
    {
        cla: "mymoney",
        name: "资金",
        push: "mymoney",
        icon: "iconfont icon-wodezijin",
        iconSelect: "iconfont icon-wodezijin"
    },
    {
        cla: "mine",
        name: "我",
        push: "user",
        icon: "iconfont icon-wo",
        iconSelect: "iconfont icon-wo"
    }
];

export {
    items
}