    export function setMychart() {
        function getStyle(obj, attr) {
            if (obj.currentStyle) {
                return obj.currentStyle[attr];
            } else {
                return document.defaultView.getComputedStyle(obj, null)[attr];
            }
        }
        //获取父元素
        var echarts = document.querySelector(".echarts");
        //获取父元素宽高
        var echartsWidth = getStyle(echarts, "width");
        var echartsHeight = getStyle(echarts, "height");

        //获取图表元素
        var myChart = document.querySelector("#myChart");

        //将父元素宽高赋值给图表
        myChart.style.width = echartsWidth;
        myChart.style.height = echartsHeight;
    }