const title = (state) => {
    return state.title
}

const name = (state) => {
    return state.name
}

const age = (state) => {
    return state.age
}

export default {
    age,
    name,
    title
}