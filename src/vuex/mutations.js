import * as types from "./mutation-types";
export default {
    [types.CHANGE_TITLE](state, title) {
        state.title = title;
    },
    [types.CHANGE_AGE](state, age) {
        state.age = age;
    },
    [types.CHANGE_NAME](state, name) {
        state.name = name;
    }
}