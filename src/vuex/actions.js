import * as types from "./mutation-types";

let changeTitle = ({ dispatch }, title) => {
    dispatch(types.CHANGE_TITLE, title);
}

let changeName = ({ dispatch }, name) => {
    dispatch(types.CHANGE_NAME, name);
}

let changeAge = ({ dispatch }, age) => {
    dispatch(types.CHANGE_AGE, age);
}

export default {
    changeAge,
    changeName,
    changeTitle
}