// eslint-disable-next-line
/* eslint-disable */
import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './vuex/index'
import 'lib-flexible/flexible.js'
import "vue2-toast/lib/toast.css"
import Toast from "vue2-toast"
import { InfiniteScroll } from 'mint-ui';

Vue.use(InfiniteScroll);
Vue.use(Toast, {
    type: 'bottom',
    duration: 3000,
    wordWrap: true,
    width: "200px",
})
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')