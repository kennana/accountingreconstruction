// eslint-disable-next-line
/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Loading from "@/views/loading.vue"
import Home from "@/components/home/component/index.vue"
import Account from "@/components/account/component/index.vue"
import AddAccount from "@/components/addaccount/component/index.vue"
import MyMoney from "@/components/mymoney/component/index.vue"
import User from "@/components/user/component/index.vue"

Vue.use(Router)

export default new Router({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/',
            name: 'loading',
            component: Loading,
            meta: {
                index: 1
            }
        },
        {
            path: '/register',
            name: 'register',
            component: () =>
                import ("@/views/register.vue"),

            meta: {
                index: 2
            }
        },
        {
            path: "/main",
            name: "main",
            redirect: '/main/home',
            component: () =>
                import ("@/views/main.vue"),
            children: [{
                path: 'home',
                name: 'home',
                component: Home
            }, {
                path: "account",
                name: "account",
                component: Account
            }, {
                path: "addaccount",
                name: "addaccount",
                component: AddAccount
            }, {
                path: "mymoney",
                name: "mymoney",
                component: MyMoney
            }, {
                path: "user",
                name: "user",
                component: User
            }]
        },
        {
            path: "*",
            name: "/notfound",
            component: () =>
                import ("@/views/notFound.vue"),
            meta: {
                index: 2
            }
        }
    ]
})