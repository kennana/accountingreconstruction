# vueaccount

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 启动页面
```
|---------loading/ 启动页面
            |----component/ 组件模块
                     |----loading.vue
            |----common/ 公共模块
            |----style/ 样式模块
```
### 注册页面
```
|---------login/ 注册页面
            |--component/ 组件模块
                   |----login.vue
            |--common/ 公共模块
            |--sytle/ 样式模块

```

### 首页
```
|---------home/ 首页
           |--component/ 组件模块
                  |----date.vue 日期组件
                  |----inout.vue 收入支出组件
                  |----visualize.vue 可视化组件
                  |----importaccout.vue 导入账单组件
                  |----voiceaccount.vue 语音账单组件
           |--common/ 公共组模块
           |--style/ 样式模块

```
### 账本
```
|---------account/ 账本
             |---component/ 组件模块
                    |-----head.vue 头部组件
                    |-----account.vue 账本组件
                    |-----scroll.vue 滚动组件
             |---common/ 公共模块
             |---style/ 样式模块

```
### 我的资金
```
|---------usermoney/ 用户资金
             |---component/ 组件模块
                    |---inputmoney.vue 输入资金组件
                    |---showmoney.vue 展示资金模块
             |---common/ 公共模块
             |---style/ 样式模块
```
### 我
```
|---------user/ 用户模块
             |---component/ 组件模块
                    |---head.vue 导航
                    |---usernote.vue 账号密码修改
                    |---dailyremind.vue 每日提醒
                    |---exportbooks.vue 导出账本
                    |---aboutus.vue 关于我们
                    |---softwareupdates.vue 软件更新
             |---common/ 公共模块
             |---style/ 样式模块
```
### 添加记账页面
```
|---------addaccount/ 添加记账
              |-----inaccount 收入页面
                       |-----component/
                                |-----head.vue 图表
                                |-----show.vue 显示各种收入
                       |-----common/
                       |-----style/
              |-----outaccount 支出页面
                       |-----component/
                                |-----head.vue 图标
                                |-----show.vue 显示各种支出
                       |-----common/
                       |-----style/

              |------transferaccounts/ 转账页面
                       |------component/
                                 |----head.vue 图标
                                 |----show.vue 显示转出转入
                       |------common/
                       |------style/

              |-------blance/ 余额
                       |------component/
                                 |---inputblance.vue 转入余额
                                 |---showblance.vue 显示余额
                       |------common/
                       |------style/
```



