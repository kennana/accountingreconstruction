注册表 / 用户表
id 主键，递增
user_name: 用户昵称
user_img: 用户头像
user_email: 用户邮箱
user_pass: 用户密码
user_confirm: 密码确认
vist_code: 用户验证码
user_token: 用户token
registered: 0 表示未注册过，1 表示注册过的，
extra: {}, 留在以后扩展其他的数据

1 有昵称是被注册过了的，提醒
2 注册过的，说明是老用户，未注册过的，说明是新用户

