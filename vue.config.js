 // eslint-disable-next-line
 /* eslint-disable */
 module.exports = {
     publicPath: process.env.NODE_ENV === 'production' ?
         '/accountingreconstruction/' : './',
     devServer: {
         port: "8081",
         open: false,
         proxy: {
             '/admin': {
                 target: "http://118.190.209.124/account/admin/",
                 changeOrigin: true,
                 pathRewrite: {
                     "^/admin": ''
                 }
             }
         }
     },
     css: {
         loaderOptions: {
             css: {},
             postcss: {
                 plugins: [
                     require('postcss-pxtorem')({
                         rootValue: 37.5,
                         selectorBlackList: ['weui', 'mu'], // 忽略转换正则匹配项
                         propList: ['*']
                     }),
                     //  require('postcss-px2rem')({ remUnit: 30 }), // 换算的基数
                 ]
             }
         }
     },
 }